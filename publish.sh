#!/bin/bash
set -e
git status

npm run build
docker buildx build --platform linux/amd64,linux/arm64,linux/arm/v7 -t cellarosi/mqttassistant:`git rev-list HEAD --count` --push .
