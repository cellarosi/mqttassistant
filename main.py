from typing import Optional

from fastapi import FastAPI, Request
from fastapi.templating import Jinja2Templates
from fastapi.staticfiles import StaticFiles



app = FastAPI()

############### Dev ###############
from fastapi.middleware.cors import CORSMiddleware

origins = [
    "http://localhost:8080"
]

app.add_middleware(
    CORSMiddleware,
    allow_origins=origins,
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)
############### Dev ###############


############### Production ###############
templates = Jinja2Templates(directory="ui/dist") 
app.mount("/css", StaticFiles(directory="ui/dist/css"), name="css")
app.mount("/js", StaticFiles(directory="ui/dist/js"), name="js")


@app.get("/")
def serve_home(request: Request):
    return templates.TemplateResponse("index.html", {"request": request})
############### Production ###############



#@app.get("/items/{item_id}")
#async def read_item(item_id: int, q: Optional[str] = None):
#    return {"item_id": item_id, "q": q}

#@app.put("/items/{item_id}/{value}")
#def update_item(item_id: int, value: int):
#    return {"item_id": item_id, "value": value}